

  /**
   * Project Modal
   */
  document.addEventListener('DOMContentLoaded', () => {
    const projectsEls = document.querySelectorAll('#projects .columns .column');
  const projectModal = document.getElementById('projectsModal');
  const projectModalBg = document.querySelector('#projectsModal .modal-background');
  const projectModalCloseBtn = document.querySelector('#projectsModal .modal-close');
  const projectModalTitle= document.querySelector('#projectsModal .modal-card-head .project-title');
  const projectModalClient = document.querySelector('#projectsModal .modal-card-head .project-client');
  const projectModalBody = document.querySelector('#projectsModal .modal-card-body');
  
  
  const closeProjectModal = () => {
    projectModal.classList.remove('is-active');
    projectModalBody.innerHTML = "";
    projectModalTitle.innerHTML = "";
    projectModalClient.innerHTML = "";
  }

  const getColumn = (title, list) => {
    const column = document.createElement('div')
    column.setAttribute('class', 'column')

    const titleEl = document.createElement('span')
    titleEl.setAttribute('class', 'is-size-5 has-text-weight-bold')
    titleEl.innerText = title;

    const ulEl = document.createElement('ul')
    ulEl.setAttribute('class', 'items ml-4')
    list && list.forEach((item) => {
        const liEl = document.createElement('li')
        liEl.setAttribute('class', 'item')
        liEl.textContent = item;
        ulEl.appendChild(liEl)
    })
    column.appendChild(titleEl);
    column.appendChild(ulEl);
    return column;

  }

  const showProjectModalContent = (params) => {
    const {title, client, details, services} = ProjectDetails[params];
    const columns = document.createElement('div')
    columns.setAttribute('class', 'columns')
    columns.appendChild(getColumn('Project Details', details))
    services && columns.appendChild(getColumn('Engineering Services', services))
    projectModalTitle.innerText = title;
    projectModalClient.innerText = client ? `Client : ${client}` : '';
    projectModalBody.appendChild(columns);

  }

  projectsEls.forEach( el => {
    el.addEventListener('click', () => {
      const targetId = el.getAttribute('id');
      projectModal.classList.add('is-active');
      showProjectModalContent(targetId);
    });
  });

  projectModalBg.addEventListener('click', closeProjectModal);
  projectModalCloseBtn.addEventListener('click', closeProjectModal);
  });
  
