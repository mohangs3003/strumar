
document.addEventListener('DOMContentLoaded', () => {
  window.scrollTo(0,500);


/**
   * Contact Us
   */ 
  const position = [12.929562, 77.542431]
  const mymap = L.map('mapid').setView(position, 13);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      maxZoom: 18
  }).addTo(mymap);

  L.marker(position).addTo(mymap)
      .bindPopup(`#60/A, 4th cross,<br />
        7th block, BSK 3rd stage,<br />
        Bengaluru - 560085,<br />
        Karanataka, India.
      `)
      .openPopup();

  
  /**
   * Capabilities Modal
   */
  const capabilitiesEls = document.querySelectorAll('#capabilities .columns .column');
  const capabilitiesModal = document.getElementById('capabilitiesModal');
  const capabilitiesModalBg = document.querySelector('#capabilitiesModal .modal-background');
  const capabilitiesModalCloseBtn = document.querySelector('#capabilitiesModal .modal-close');
  const titleEl = capabilitiesModal.querySelector('.title')
  const infoEl = capabilitiesModal.querySelector('.info')

  const closeCapabilitiesModal = () => {
    capabilitiesModal.classList.remove('is-active');
    infoEl.innerHTML = "";
  }

  const showCapabilitiesModalContent = (params) => {
    const {title, details} = CapabilityDetails[params];
    
    titleEl.innerText = title;

    const ulEl = document.createElement('ul')
    ulEl.setAttribute('class', 'items')
    details.forEach((detail) => {
        const liEl = document.createElement('li')
        liEl.setAttribute('class', 'item')
        liEl.textContent = detail;
        ulEl.appendChild(liEl)
    })
    infoEl.appendChild(ulEl);
  }

  capabilitiesEls.forEach( el => {
    el.addEventListener('click', () => {
      const targetId = el.getAttribute('id')
      capabilitiesModal.classList.add('is-active');
      showCapabilitiesModalContent(targetId);
    });
  });
  capabilitiesModalBg.addEventListener('click', closeCapabilitiesModal);
  capabilitiesModalCloseBtn.addEventListener('click', closeCapabilitiesModal);

  /**
   * Project Page 
   */
  const projectCards = document.querySelectorAll('.project-card');
  projectCards.forEach( el => {
    el.addEventListener('click', (event) => {
      console.log('id', event);
      // console.log('onshore-projectCards.html ', );
      location.href = `${window.location.origin}/offshore-projects.html`;
    });
  });
  

});
