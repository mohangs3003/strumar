document.addEventListener('DOMContentLoaded', () => {
    const navbarBurger = document.getElementById('menuBarBurger');
  
    navbarBurger.addEventListener('click', () => {
        navbarBurger.classList.toggle('is-active');
        document.getElementById(navbarBurger.dataset.target).classList.toggle('is-active');
    });

  
  const $navItems = document.querySelectorAll('.navbar-item');
    $navItems.forEach( el => {
      el.addEventListener('click', () => {
        const $target1 = document.getElementById('menuBarBurger');
        const $target2 = document.getElementById('menuBar');
        $target1.classList.remove('is-active');
        $target2.classList.remove('is-active');
      });
    });


    const navItems = document.getElementById('menuBar').firstElementChild.children,
          navSections = new Array(navItems.length);

    for (i = 0; i < navItems.length; i++)
        navSections[i] = document.getElementById(navItems[i].dataset.target);

    const menuBarHeight = document.getElementById('menuBar').offsetHeight;
    function isVisible(ele) {
        const r = ele && ele.getBoundingClientRect();
        const h = (window.innerHeight || document.documentElement.clientHeight);
        const w = (window.innerWidth || document.documentElement.clientWidth);
        return (r.top <= h) && 
            (r.top + r.height - menuBarHeight >= 0) && 
            (r.left <= h) && 
            (r.left + r.width >= 0);
    }
    function activateIfVisible() {
        for (b = true, i = 0; i < navItems.length; i++) {
            if (b && isVisible(navSections[i])) {
                navItems[i].classList.add('is-active');
                b = false;
            } else 
                navItems[i].classList.remove('is-active');
        }
    }
    var isTicking = null;
    window.addEventListener('scroll', () => {
      var $nav = document.getElementsByClassName("navbar")[0];
      const viewHeight = document.body.scrollTop || document.documentElement.scrollTop;
      const navbarHeight = 200;
      if(viewHeight >= navbarHeight){
        $nav.classList.add('scrolled');
      }else{
        $nav.classList.remove('scrolled');
      }

      if (!isTicking) {
          window.requestAnimationFrame(() => {
              activateIfVisible();
              isTicking = false;
          });
          isTicking = true;
      }
    }, false);

    // var isScrolling = null;
    // window.addEventListener('scroll', () => {
    //     window.clearTimeout(isScrolling);
    //     isScrolling = setTimeout(() => {
    //         activateIfVisible();
    //     }, 80);
    // }, false);

    for (item of navItems) {
      item.addEventListener('click', e => {
          e.preventDefault();
          window.scroll({ 
              behavior: 'smooth', 
              left: 0, 
              top: document.getElementById(e.target.dataset.target).getBoundingClientRect().top + 
                  window.scrollY - 110
          });
      });
  }
});
