const CapabilityDetails = {
    'offshore': {
        'title': 'Offshore structural engineering includes :',
        'details': ['Inservice analysis & design of offshore structures - Dynamic inplace, Spectral fatigue, Seismic analyses for fixed platforms.', 'Preservice analysis and design of offshore structures - Lift, Loadout (Trailer & Skidded), Transportaion, Launch analyses for fixed platforms.', 'Floatover structural analyses', 'Analysis & Design of FPSO topsides.', 'Advanced structural analysis for accidential loadings - Boat Impact, Blast.', 'Ulitimate Strength Analysis.', 'Brownfield modification engineering.', 'Cathodic Protection design.']
    },
    'installation': {
        'title': 'Installation engineering includes :',
        'details': ['Analysis & Design of offshore structures - lift, loadout (Trailer & Skided), Transportation, Launch analyses for fixed platforms.', 'Loadout trailer & seafastening arrangement.', 'Transportation & Grillage arrangement.', 'Onbottom stability analysis.', 'Pile - Drivability, Stickup, Fatigue & Handling analyses & design.']
    },
    'subsea': {
        'title': 'Subsea engineering includes :',
        'details': ['Structural analysis and design of PLET, PLEM, ILT, Manifolds for installation and inservice conditions.', 'Mudmat analysis & designs.', 'Anchor forge analysis & designs.', 'Design of installation aids such as Yoke, Padeyes, Trunions, Spreaderbar, Cathodic protection & miscellaneous items for subsea structures.', 'Droped object studies.', 'ROV operation studies.']
    },
    'onshore': {
        'title': 'Onshore Structural engineering includes :',
        'details': ['Design of Technological structures for oil & gas industry.', 'Design of Manufacturing, Warehouse, Food processing industrial structures.', 'Equipment supporting skids & foundation designs.']
    },
    'reassesment': {
        'title': 'Structural Integrity Mangement (SIM) includes :',
        'details': ['Reassesment & Life extension studies of offshore platforms.', 'Inplace, Spectral Fatigue, Nonlinear pushover, Boat-impact analysis for fixed offshore platforms.']
    },
    'decommissioning': {
        'title': 'Decommissioning engineering include :',
        'details': ['Decommissioning sequence.', 'Stability analysis', 'Engineering of jacket & topsides removal, which includes lift, transportation & loadin analyses.']
    }
};

const ProjectDetails = {
    project_1: {
        title: 'UMM SHAIF',
        location: 'Middle East',
        client: 'ADNOC',
        details: ['Reassesment of Platform', 'No. of Platform - 5', 'Water depth 13.0 m - 29.0 m'],
        services: ['Deck extension', 'Inplace analysis', 'Spectral fatigue', 'Nonlinear pushover', 'Analysis report', 'Weight Control report', 'Software used - SACS']
    },
    project_3: {
        title: 'QSA - 6',
        location: 'Middle East',
        client: 'ADNOC',
        details: ['Reassesment of Complex', 'No. of Jackets - 2', 'Water depth - 13.0 m', 'Topside weight - 28,000 MT'],
        services: ['Deck superelement', 'Inplace analysis', 'Nonlinear pushover', 'Analysis report']
    },
    project_5: {
        title: 'KG - DWN 98/2',
        location: 'India',
        client: 'ONGC',
        details: ['Central processing platform', 'CPP topside skidded loadout weight - 15,000 MT', 'PGC module weight - 3,250 MT'],
        services: ['PGC module lifting analysis', 'CPP topside skidded loadout without PGC module', 'Miscellaneous design and reports', 'Analysis reportAnalysis and design report', 'Weight control report']
    },
    project_4: {
        title: 'QSA - 6',
        location: 'Middle East',
        client: 'ADNOC',
        details: ['Reassesment of Complex', 'No. of Jackets - 2', 'Water depth - 13.0 m', 'Topside weight - 28,000 MT'],
        services: ['Deck superelement', 'Inplace analysis', 'Nonlinear pushover', 'Analysis report']
    },
    project_9: {
        title: 'FEED - WHP',
        location: 'Nigeria',
        client: 'Shell',
        details: ['FEED for wellhead topside and jacket', 'Water depth - 96.0 m', 'Jacket weight - 3500 MT', 'Topside weight - 1500 MT'],
        services: ['Inservice & Preservice analyses', 'Analysis and Design report']
    },
    project_2: {
        title: 'QSA - 6',
        location: 'Middle East',
        client: 'ADNOC',
        details: ['Reassesment of Complex', 'No. of Jackets - 5', 'Water depth - 13.0 m', 'Topside weight - 28,000 MT'],
        services: ['Deck superelement', 'Inplace analysis', 'Nonlinear pushover', 'Analysis report']
    },
    project_6: {
        title: 'Compressor Skid',
        location: 'Singapore',
        client: 'S3G energy solutions',
        details: ['Detail engineering of compressor skid', 'Lift weight - 120 MT'],
        services: ['Inplace analysis', 'Lift analysis', 'Miscellaneous design and reports', 'Analysis and design report', 'Weight control report']
    },
    project_7: {
        title: 'Refrigeration Skid',
        location: 'Singapore',
        client: 'S3G energy solutions',
        details: ['Detail engineering of Refrigeration skid', 'Lift weight - 140 MT'],
        services: ['Inplace analysis', 'Lift analysis', 'Transportation analysis', 'Miscellaneous design and reports', 'Analysis and design report', 'Weight control report']
    },
    project_8: {
        title: 'Conceptual Engineering – FPSO Topside',
        location: '',
        client: '',
        details: ['Conceptual engineering of compressor module']
        // services: []
    }

};